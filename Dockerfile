#
# Red5 + OpenJDK 7 Dockerfile
#
# https://github.com/Red5/docker
#

FROM adoptopenjdk/openjdk14

MAINTAINER Anos Mhazo <anosmhazo@gmail.com>

ENV DEBIAN_FRONTEND noninteractive
ENV RED5_HOME /opt/red5
ENV RED5_VERSION 1.2.3

WORKDIR /tmp

RUN apt-get update && apt-get install -yq wget apt-utils && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN wget -q https://github.com/Red5/red5-server/releases/download/v${RED5_VERSION}/red5-server-${RED5_VERSION}.tar.gz && \
    tar -xzf red5-server-${RED5_VERSION}.tar.gz && \
    mv red5-server /opt/red5 && \
    rm red5-server-${RED5_VERSION}.tar.gz

VOLUME ["/opt/red5"]

EXPOSE 843 1935 5080 5443 8081 8443

WORKDIR /opt/red5

ENTRYPOINT ["./red5.sh"]
